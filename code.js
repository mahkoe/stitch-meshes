var last_tm = 0;
var total_tm = 0;
var anim_frame_handle = null;

var cvs = null;
var ctx = null;

var mouseX = 0, mouseY = 0;
var pmouseX = 0, pmouseY = 0;
var dmouseX = 0, dmouseY = 0;
var mouseIsPressed = false;
var mouseMoved = false;

var update_mouse = function(e) {
    if (
        (e.clientX < 0) || (e.clientX >= cvs.width) ||
        (e.clientY < 0) || (e.clientY >= cvs.height)
    ) {
        return;
    }
    
    pmouseX = mouseX;
    pmouseY = mouseY;
    mouseX = e.clientX;
    mouseY = e.clientY;
    dmouseX = mouseX - pmouseX;
    dmouseY = mouseY - pmouseY;

    mouseMoved = true;
}

//The eye (i.e. camera) is at <0,0,eye_z> looking towards <0,0,0>
//The world coordinates of the screen are defined by the box whose
//top-left corner is <-0.5,0.5,screen_z>, and bottom right is at 
//<0.5,-0.5,screen_z>
var eye_z = -12;
var screen_z = -10; //Adjusting this changes FOV
//Cache these values to save performance
var cvs_w = 400;
var cvs_h = 400;


var vertices = [];
var faces = [];

/**========== VECTOR/MATRIX FUNCTIONS ==========**/

//I use the 4x4 matrix trick, but I have optimized all
//these functions to assume that vector w components are
//always 1 and that the bottom row of all matrices is
//[0,0,0,1]
var matvec = function(m,v) {
    return [
        m[0]*v[0] + m[1]*v[1] + m[2]*v[2] + m[3],
        m[4]*v[0] + m[5]*v[1] + m[6]*v[2] + m[7],
        m[8]*v[0] + m[9]*v[1] + m[10]*v[2] + m[11],
    ];
};

var mat3vec = function(m,v) {
    return [
        m[0]*v[0] + m[1]*v[1] + m[2]*v[2],
        m[4]*v[0] + m[5]*v[1] + m[6]*v[2],
        m[8]*v[0] + m[9]*v[1] + m[10]*v[2],
    ];
};

var matmat = function(m1,m2) {
    var col1 = mat3vec(m1,[m2[0], m2[4], m2[8]]);
    var col2 = mat3vec(m1,[m2[1], m2[5], m2[9]]);
    var col3 = mat3vec(m1,[m2[2], m2[6], m2[10]]);
    var col4 = mat3vec(m1,[m2[3], m2[7], m2[11]]);
    
    return [
        col1[0], col2[0], col3[0], col4[0] + m1[3],
        col1[1], col2[1], col3[1], col4[1] + m1[7],
        col1[2], col2[2], col3[2], col4[2] + m1[11]
    ];
};

var xyz_transform = function(pt) {
    var scale = (screen_z - eye_z) / (pt[2] - eye_z)
    return [
        (pt[0]*scale+0.5)*cvs_w,
        -(pt[1]*scale-0.5)*cvs_h,
        pt[2]
    ];
}

//Helper that uses degrees instead of radians
var cos = function(x) {
    return Math.cos(180 * x / Math.PI);
}
var sin = function(x) {
    return Math.sin(180 * x / Math.PI);
}

//Spins around y axis CCW by phi degrees
var yaw_mat = function(phi) {
    var c = cos(phi);
    var s = sin(phi);
    return [
         c, 0, s, 0,
         0, 1, 0, 0,
        -s, 0, c, 0
    ];
};

//Spins around x axis CCW by phi degrees
var roll_mat = function(phi) {
    var c = cos(phi);
    var s = sin(phi);
    return [
        1, 0,  0, 0,
        0, c, -s, 0,
        0, s,  c, 0
    ];
};

var xlate_mat = function(v) {
    return [
        1, 0, 0, v[0],
        0, 1, 0, v[1],
        0, 0, 1, v[2]
    ];
};

//TODO: figure out nicer way to look at model (zoom, pan, rotate around
//point right in front of the camera)

var model_mat = [
    1, 0, 0, 0,
    0, 1, 0, 0,
    0, 0, 1, 0
];

var draw = function(tm) {
    var dt = tm - last_tm;
    //console.log("dt = ", dt)
    last_tm = tm;
    total_tm += dt;

    cvs_w = cvs.width;
    cvs_h = cvs.height;

    if (!mouseMoved) {
        pmouseX = mouseX;
        pmouseY = mouseY;
        dmouseX = 0;
        dmouseY = 0;
    }
    
    if (mouseIsPressed) {
        var roll = roll_mat(-dmouseY * 0.0008);
        var yaw = yaw_mat(-dmouseX * 0.0008);
        model_mat = matmat(
            roll,
            matmat(yaw, model_mat)
        );
    }
    
    var verts_xformed = new Array();
    for (const v of vertices) {
        verts_xformed.push(
            xyz_transform(matvec(model_mat, v))
        );
    }

    ctx.clearRect(0,0,cvs_w,cvs_h);

    ctx.strokeStyle = "black";
    ctx.lineWidth = 0;
    for (const f of faces) {
        ctx.beginPath();
        var first = true;
        for (const idx of f) {
            var pt = verts_xformed[idx];
            if (first) {
                ctx.moveTo(pt[0],pt[1]);
                first = false;
            } else {
                ctx.lineTo(pt[0],pt[1]);
            }
        }

        ctx.closePath();
        ctx.stroke();
    }
    
    //Cover our butts by eventually stopping animations
    /*if (total_tm < 100000)*/ window.requestAnimationFrame(draw);

    mouseMoved = false;
}

var start_anim = function() {
    anim_frame_handle = window.requestAnimationFrame(function(tm){
        last_tm = tm;
        anim_frame_handle = window.requestAnimationFrame(draw);
    });
}

var go = function() {
    const fileInput = document.getElementById("thefile");
    const output = document.getElementById("output");

    cvs = document.getElementById("cvs");
    ctx = cvs.getContext("2d");

    cvs.addEventListener("mousemove", update_mouse);
    cvs.addEventListener("mousedown", function(e) {
        mouseIsPressed = (e.buttons & 1) != 0;
    });
    cvs.addEventListener("mouseup", function(e) {
        mouseIsPressed = (e.buttons & 1) != 0;
    });
    
    fileInput.addEventListener("change", ()=>{
        for (const file of fileInput.files) {
            vertices = new Array();
            faces = new Array();
            file.text().then(function(txt) {
                var vertex_re = /v (\S+) (\S+) (\S+)/g
                for (const match of txt.matchAll(vertex_re)) {
                    var v = new Array(3);
                    v[0] = parseFloat(match[1]);
                    v[1] = parseFloat(match[2]);
                    v[2] = parseFloat(match[3]);
                    vertices.push(v);
                }

                var face_re = /^f (.*)$/gm;
                var el_re = /(\d+)\S*\s*/g
                for (const match of txt.matchAll(face_re)) {
                    var f = new Array();
                    for (const el_match of match[1].matchAll(el_re)) {
                        f.push(parseInt(el_match[1]) - 1)
                    }
                    
                    faces.push(f);
                }
            });
        }
        console.log("vertices", vertices);
        console.log("faces", faces);
    }
    );

    start_anim();
}
    
var save_file = function() {
    const blarg = "Hi this my file that I generated";
    var file = new Blob([blarg]);
    var a = document.createElement("a")
      , url = URL.createObjectURL(file);
    console.log("URL = ", url);
    a.href = url;
    a.download = "generated_file.txt";
    a.click();
    //setTimeout(function() {
        a.remove();
        URL.revokeObjectURL(url);
    //}, 0);
}